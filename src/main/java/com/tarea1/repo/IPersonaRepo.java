package com.tarea1.repo;

import com.tarea1.model.Persona;

public interface IPersonaRepo extends IGenericRepo<Persona, Integer> {

}
