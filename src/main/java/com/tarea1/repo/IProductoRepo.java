package com.tarea1.repo;

import com.tarea1.model.Producto;

public interface IProductoRepo extends IGenericRepo<Producto, Integer> {

}
