package com.tarea1.repo;

import com.tarea1.model.Venta;

public interface IVentaRepo extends IGenericRepo<Venta, Integer> {

}
