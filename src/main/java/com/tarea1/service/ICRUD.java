package com.tarea1.service;

import java.util.List;

public interface ICRUD<T, ID> {
	List<T> listar();

	T listarPorId(ID id);

	T registrar(T obj);

	T modificar(T obj);

	void eliminar(ID id);
}
