package com.tarea1.service;

import com.tarea1.model.Venta;

public interface IVentaService {
	Venta listarPorId(Integer id) throws Exception;
	Venta crear(Venta venta) throws Exception;
}
