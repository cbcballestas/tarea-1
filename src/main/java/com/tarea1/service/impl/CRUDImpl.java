package com.tarea1.service.impl;

import java.util.List;

import com.tarea1.repo.IGenericRepo;
import com.tarea1.service.ICRUD;

public abstract class CRUDImpl<T, ID> implements ICRUD<T, ID> {

	protected abstract IGenericRepo<T, ID> getRepo();

	@Override
	public List<T> listar() {
		return getRepo().findAll();
	}

	@Override
	public T listarPorId(ID id) {
		return getRepo().findById(id).orElse(null);
	}

	@Override
	public T registrar(T obj) {
		return getRepo().save(obj);
	}

	@Override
	public T modificar(T obj) {
		return getRepo().save(obj);
	}

	@Override
	public void eliminar(ID id) {
		getRepo().deleteById(id);
	}
}
