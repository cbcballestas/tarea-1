package com.tarea1.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tarea1.model.Venta;
import com.tarea1.repo.IVentaRepo;
import com.tarea1.service.IVentaService;

@Service
public class VentaServiceImpl implements IVentaService {

	@Autowired
	private IVentaRepo repo;

	@Override // Realización de inserción en venta y detalle_venta
	public Venta crear(Venta venta) throws Exception {
		venta.getDetalleVenta().forEach(det -> det.setVenta(venta));
		return repo.save(venta);
	}

	@Override
	public Venta listarPorId(Integer id) {
		return repo.findById(id).orElse(null);
	}

}
