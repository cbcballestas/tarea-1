package com.tarea1.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.tarea1.exception.ModeloNotFoundException;
import com.tarea1.model.Venta;
import com.tarea1.service.IVentaService;

@RestController
@RequestMapping("/ventas")
public class VentaController {

	@Autowired
	private IVentaService service;

	@PostMapping // Nivel 2 Richardson
	public ResponseEntity<Void> guardar(@Valid @RequestBody Venta venta) throws Exception {
		Venta obj = service.crear(venta);
		// localhost:8080/ventas/{id}
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getIdVenta())
				.toUri();
		return ResponseEntity.created(location).build();
	}

	@GetMapping("/{id}")
	public ResponseEntity<Venta> listarPorId(@PathVariable Integer id) throws Exception {
		Venta obj = service.listarPorId(id);

		if (obj == null) {
			throw new ModeloNotFoundException("ID VENTA NO ENCONTRADO: " + id);
		}

		return new ResponseEntity<Venta>(obj, HttpStatus.OK);
	}

}
